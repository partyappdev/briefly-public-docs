# REST API

## Общая информация
Делаем в этот раз скорее не-рест, а RPC-style API. Это даст нам гибкости в вопросах безопасности, и избавит от ряда неочевидных проблем (вроде необходимости неизменного стейта на GET запросах). Так что эндпоинт максимально простой, а параметры все укладываем в body.

В каждом запросе в хидере будет приложен API_KEY. По этому API Key надо вернуть app_id (как раньше из когнито айди юзер айди). Сейчас пускай функция, которая превращает API_KEY в app_id возвращает захардкоженное ff000000 (айди для ребят из FamiltyFriend) либо aa000000 (это для наших тестов). Но ключи проверяем!

Тестовые ключи:

публичный ключ (для iOS клиента)
pk_test_08b66b6a93d44d7a9a428c3918b2e1f0

приватный ключ (для запросов севрерных - автотестов)
sk_test_20bcd1daeb024e0da79eb2904a4f25e0

Dev endpoint:
https://t823weq3t5.execute-api.us-east-1.amazonaws.com/v1

Staging endpoint:
https://ntmjcdv5ia.execute-api.us-east-1.amazonaws.com/v1

Prod endpoint:
https://d4757l7yqk.execute-api.us-east-1.amazonaws.com/v1
