# FamilyFriend adapter

Временный адаптер, транслирующий аналитику с FF во внутренний формат. В будущем адаптеры отсюда обобщим.

## POST adapters/ff_post_daily_data
#### request body:
```
{
	"date_from": "2017-04-23T23:00:00.000Z",
	"date_to": "2017-04-23T23:59:59.999Z",
	"stats": [
		{
			"item_id": "11221",
			"group_id": "039840"
			"name": "Огурцы на развес",
			"total_sold": 123,
			"total_sold_units": None,
			"residue": 2341,
			"residue_units": "кг",
			"rejects": 323,
			"rejects_units": None,
			"storepoint_name": "РЦ Складочная 16",
			"storepoint_id": 1323,
			"sales_currency": "USD",
			"sales_value": 3143,
			"disposals_value": 1001,
			"margin": 0.02,
		}
	]
}
```
* **date_from** - дата начала периуда
* **date_to** - дата окончания периуда
* **stats.item_id** - айди айтема (в конкретном магазине)
* **stats.group_id** - глобальный айди для конкретного item_id (если не сложно его найти - пусть пока прикапываются у нас. В дальнейшем проще будет сделать глобальные выборки).
* **stats.name** - название продукта для отображения в результатах
* **stats.total_sold** - продажи за день
* **stats.total_sold_units** - в каких единицах шлем (если в штуках то сюда кладем None)
* **stats.residue** - остатки на конец дня
* **stats.residue_units** - в каких единицах шлем (если в штуках то сюда кладем None)
* **stats.rejects** - количество отказов на конец дня
* **stats.rejects_units** - в каких единицах шлем (если в штуках то сюда кладем None)
* **stats.storepoint_name** - название РЦ для этого айтема
* **stats.storepoint_id** - айди РЦ для этого айтема
* **stats.rc_name** - название РЦ для этого айтема
* **stats.rc_id** - айди РЦ для этого айтема
* **stats.sales_currency** - в какой валюте делаются продажи (USD/RUB/etc.)
* **stats.sales_value** - продажи в деньгах (инт). С точночтью до центов/копеек. Т е $2.51 -> 251. 12.1руб -> 1210
* **stats.disposals_value** - списания в деньгах (инт). В той же валюте, что и sales
* **stats.margin** - маржа на этом продукте

#### response body:
```
{
	"status": "succeed"
}
```
