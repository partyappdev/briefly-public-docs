# Heatmaps api
Этот кусман API отвечает за построение тепловых карт тычков. Пока в самом примитивном виде.

## POST heatmaps/add_touches
#### request body:
```
{
    "user_id": "032901",
    "client_id": "ios-client",
    "client_version": "1.0.0",
    "device_id": "iphone8.1",
    "date": "2017-04-23T18:25:43.511Z",
    "touches": [
        {
            "orientation": "vertical-compact#horisontal-compact",
            "view_group_id": "MainViewController",
            "view_id": "MainViewController/UIScrollView",
            "w": 320,
            "h": 647,
            "points": [
                {
                    "x": 326,
                    "y": 220
                    "t": "tap"
                },
                ...
            ]
        },
        ...
    ]
}
```
* **user_id** - id пользотваеля
* **client_version** - версия клиента
* **device_id** - тип устрйоства пользуовтеля
* **date** - дата, когда сформирован пакет тачей
* **touches** - массив пакетов тычков
* **touches[i].orientation** - ориентация девайса. Строковая константа
* **touches[i].view_id** - идентификатор вида, в котором был сделан тычек
* **touches[i].w** - ширина вида в точках (НЕ ПИКСЕЛЯХ!!)
* **touches[i].h** - высота вида в точках (НЕ ПИКСЕЛЯХ!!)
* **touches[i].points** - тычки
* **touches[i].points[j].x** - координата тычка
* **touches[i].points[j].y** - координата тычка
* **touches[i].points[j].t** - тип тычка

#### response body:

В случае успеха:
```
{
    "status": "succeed"
}
```
#### Implementation details:

Суть такая: раскидываем эти тычки по картам. Каждая из карт уникально идентифицируется таким образом: *app_id/client_id/client_version/view_id/orientation/dimension_configuration*.

Все первые аргументы берутся из запроса. *dimension_configuration* вычисляется так: 

*dimension_configuration* = *w* / *h* в виде несократимой дроби (пока сойдет)

Ну а дальше дело техники - в базе лежит для вью частотная карта (размер - 128*128) Каждая точка - количество тычков в это точку вьюхи. Присовокупили новые данные - и залили в базу.
**TODO:** как попрут большие данные столкнемся с тем, что база будет валиться на транзакциях. Надо будет организовать очередь запросов к картам и выполнять их по очереди, чтоб не дрючить синхронизацию на уровне базы


## POST heatmaps/get_heatmap
#### request body:
```
{
    "client_id": "ios-client",
    "client_version": "1.0.0",
    "view_id": "MainViewController/UIScrollView",
    "view_width": 320,
    "view_height": 400,
    "orientation": "vertical-compact#horisontal-compact"
    "touch_type": "tap" или "pan". По умолчанию "tap"
}
```
* **client_version** - версия клиента
* **touches** - массив пакетов тычков
* **orientation** - ориентация девайса. Строковая константа
* **view_id** - идентификатор вида, для которого нужно вернуть хитмап
* **view_width** - ширина вида в точках (НЕ ПИКСЕЛЯХ!!)
* **view_height** - высота вида в точках (НЕ ПИКСЕЛЯХ!!)

#### response body:

В случае успеха:
```
{
    "heatmaps": [
        {
            "view_id": "MainViewController/UIScrollView",
            "view_width": 320,
            "view_height": 400,
            "points_in_row": 128,
            "map": "1,0,0,0,1,0,0,0,0,0,0,0,0..."
        }
    ]
}
```

* **heatmaps** - массив карт (сейчас вовзращает толкьо одну запрошенную эту)
* **heatmaps[i].view_id** - айди запрошенного вида
* **heatmaps[i].view_width** - ширина вида в точках (НЕ ПИКСЕЛЯХ!!)
* **heatmaps[i].view_height** - высота вида в точках (НЕ ПИКСЕЛЯХ!!)
* **heatmaps[i].points_in_row** - количество точке в одной строке двумерного массива
* **heatmaps[i].map** - через запятую лежат чиселки карты. Каждые **points_in_row** начинается новая строка двумерного массива:

Тут вроде вообще все тупее не бывает - составили на базе запроса айди карты - нашли ее в базе - заэнкодили в base64 и вернули

