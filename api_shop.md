# Shoping api:

## POST shop/add_to_cart
#### in:
{"user_id", "item_id", "count", "timestamp", "one_item_cost"}
#### out:
{"status": "succeed"}
#### как рабоатет:
+ получаем **app_id**
+ По **user_id** берем текущий **cart_id**. Если **cart_id** не задан - то генерируем и записываем его в **statsapp_{env}_user_state**
+ Добавляем запись в таблицу **statsapp_{env}_all_events** запись (вроде аргиментов должно хватить чтоб заполнить - все что не влазит - записываем в arguments - но желательо не потерять никакую инфу)
+ Из таблицы **statsapp_{env}_shop_cart** по **cart_id** и **item_id** получаем энтити. Если этого энтити нет - то создаем пустую. Далее увеличиваем атомарно count,  на значение, переданное параметром (пробуем это сделать несоклько раз, пока не будет успех)

## POST shop/remove_from_cart

#### in:
{"user_id", "item_id", "count", "timestamp", "one_item_cost"}
#### out:
{"status": "succeed"}
#### как рабоатет:
+ получаем app_id
+ По user_id берем текущий cart_id. Если cart_id не задан - то генерируем и записываем его в statsapp_{env}_user_state
+ Добавляем запись в таблицу statsapp_{env}_all_events запись (вроде аргиментов должно хватить чтоб заполнить - все что не влазит - записываем в arguments - но желательо не потерять никакую инфу)
+ Из таблицы statsapp_{env}_shop_cart по card_is и item_id получаем энтити. Если этого энтити нет - то игнорируем и логгируем fatal ошибку. Если есть, то уменьшаем атомарно count,  на значение, переданное параметром  (пробуем это сделать несоклько раз, пока не будет успех)

## POST shop/make_purchase

#### in:
{"user_id", "timestamp", "purchase_id"}
#### out:
{"status": "succeed"}
#### как рабатает:
+ Получаем app_id
+ По user_id берем текущий cart_id. Если cart_id не задан - то возвращаем ошибку
+ Добавляем запись в таблицу statsapp_{env}_all_events запись (вроде аргиментов должно хватить чтоб заполнить - все что не влазит - записываем в arguments - но желательо не потерять никакую инфу)
+ Создаем запись в statsapp_{env}_shop_purchases (в стейте 1)
+ Далее перечисляем все итемы из cart_id. Для каждого item_id идем в statsapp_{env}_shop_statistic_by_item и ищем записи сгенерив айди (один месячный другой дневной) на базе таймстемпа item_id и app_id. Если какой либо из записей не сущетсвует, то создаем ее (пустую). Далее делаем апдейт этих айтемов, атомарно увеличив total_purchases на count из cart, и total_income на count *one_item_price из cart. Таким же макаром забиваем в
+ Юзеру в statsapp_{env}_user_state выставляем current_cart_id в None

## POST shop/cancel_purchase

#### in:
{"user_id", "timestamp", "purchase_id"}
#### out:
{"status": "succeed"}
#### как рабатает:
+ Получаем app_id
+ По purchase_id берем текущий cart_id. Если cart_id не задан - то возвращаем ошибку
+ Добавляем запись в таблицу statsapp_{env}_all_events запись (вроде аргиментов должно хватить чтоб заполнить - все что не влазит - записываем в arguments - но желательо не потерять никакую инфу)
+ Ищем запись в statsapp_{env}_shop_purchases. Меняем ей стейт на -1.
+ Далее перечисляем все итемы из cart_id. Для каждого item_id идем в statsapp_{env}_shop_statistic_by_item и ищем записи сгенерив айди (один месячный другой дневной) на базе таймстемпа item_id и app_id. Если одной из записей не сущетсвует, то пишем в лог ошибку и идем на след item. Если же все ок - делаем апдейт этоих айтемов, атомарно уменьшив total_purchases на count из cart, и total_income на count *one_item_price из cart.

## POST shop/items/statistics

пока поддерживаем толкьо статистику типо "last_month_total"
#### in:
{"values": ["last_month_total","last_month_total_income"], "item_id"}
#### out:
{"last_month_total": 100}
#### как рабоатет:
+ Получаем app_id
+ Идем в таблицу statsapp_{env}_shop_statistic_by_item и вычислям 30 айдишников последних дней (не включая сегодняшний) - берем 30 дневных записей и проссумировав значения выдаем результат
